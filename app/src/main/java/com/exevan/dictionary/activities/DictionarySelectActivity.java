package com.exevan.dictionary.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.exevan.dictionary.R;
import com.exevan.dictionary.adapter.DictListAdapter;
import com.exevan.dictionary.tasks.DictionaryListTask;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class DictionarySelectActivity extends AppCompatActivity {

    Map<String, String> allDicts;
    List<String> selectedDicts;

    //--------ONCREATE------------------------------------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dictionary_select);

        initDictList();
    }

    private void initDictList() {
        this.allDicts = getDicts();
        this.selectedDicts = new ArrayList<>(allDicts.keySet());

        ListView dictList = (ListView) findViewById(R.id.dictlist);
        dictList.setAdapter(new DictListAdapter(this, allDicts));

    }

    private Map<String, String> getDicts() {
        try {
            return new DictionaryListTask().execute().get();
        } catch (InterruptedException |ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    //--------ONBACKPRESSED-------------------------------------------------------------------------

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();

        String[] ids = new String[selectedDicts.size()];
        String[] names = new String[selectedDicts.size()];

        for (int i = 0; i < selectedDicts.size(); i++) {
            ids[i] = selectedDicts.get(i);
            names[i] = allDicts.get(selectedDicts.get(i));
        }

        intent.putExtra(SELECTED_DICTS_IDS, ids);
        intent.putExtra(SELECTED_DICTS_NAMES, names);
        setResult(RESULT_OK, intent);
        finish();
    }

    //--------SELECT--------------------------------------------------------------------------------

    public void selectDict(final String id, View view) {

        if (view != null) {
            view.setBackground(getResources().getDrawable(R.drawable.item_dictlist_selectedborder));
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    unselectDict(id, v);
                }
            });
        }

        selectedDicts.add(id);
    }

    public void unselectDict(final String id, View view) {

        if (view != null) {
            view.setBackground(null);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectDict(id, v);
                }
            });
        }

        selectedDicts.remove(id);
    }

    public static final String SELECTED_DICTS_IDS = "selected_dicts_ids";
    public static final String SELECTED_DICTS_NAMES = "selected_dicts_names";
}
