package com.exevan.dictionary.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

import com.exevan.dictionary.R;
import com.exevan.dictionary.adapter.DefListAdapter;
import com.exevan.dictionary.adapter.DictListAdapter;
import com.exevan.dictionary.tasks.DictionaryListTask;
import com.exevan.dictionary.tasks.LookupTask;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class LookupActivity extends AppCompatActivity {

    Map<String, String> selectedDicts;

    //--------ONCREATE------------------------------------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lookup);
        selectedDicts = getDicts();
    }

    private Map<String, String> getDicts() {
        try {
            return new DictionaryListTask().execute().get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    //--------ONACTIVITYRESULT----------------------------------------------------------------------

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUESTCODE_DICTIONARYSELECT:
                String[] ids = data.getStringArrayExtra(DictionarySelectActivity.SELECTED_DICTS_IDS);
                String[] names = data.getStringArrayExtra(DictionarySelectActivity.SELECTED_DICTS_NAMES);
                handleDictionarySelectResult(ids, names);
                break;
            default:
                break;
        }
    }

    public void launchDictionarySelectActivity(View view) {
        Intent intent = new Intent(this, DictionarySelectActivity.class);
        startActivityForResult(intent, REQUESTCODE_DICTIONARYSELECT);
    }

    private void handleDictionarySelectResult(String[] ids, String[] names) {
        selectedDicts = new HashMap<>();
        for (int i = 0; i < ids.length; i++) {
            selectedDicts.put(ids[i], names[i]);
        }
    }

    //--------LOOKUP--------------------------------------------------------------------------------

    public void lookup(View view) {
        String term = ((EditText) findViewById(R.id.lookuptxt)).getText().toString();
        Map<String, String> definitions = getDefs(term);

        populateDefList(definitions);
    }

    private Map<String, String> getDefs(String term) {
        try {
            return new LookupTask(selectedDicts).execute(term).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void populateDefList(Map<String, String> defs) {
        ListView dictList = (ListView) findViewById(R.id.deflist);
        dictList.setAdapter(new DefListAdapter(this, defs));
    }

    public static final int REQUESTCODE_DICTIONARYSELECT = 1;

}
