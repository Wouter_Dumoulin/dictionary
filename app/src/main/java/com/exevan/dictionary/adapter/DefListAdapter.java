package com.exevan.dictionary.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.exevan.dictionary.R;
import java.util.Map;

/**
 * Created by Exevan on 27/01/2017.
 */

public class DefListAdapter extends BaseAdapter {

    private Context context;

    private String[] names;
    private String[] defs;


    public DefListAdapter(Context context, Map<String, String> defs) {
        this.context = context;
        this.names = defs.keySet().toArray(new String[defs.size()]);
        this.defs = new String[defs.size()];
        for (int i = 0; i < defs.size(); i++) {
            this.defs[i] = defs.get(this.names[i]);
        }
    }

    @Override
    public int getCount() {
        return names.length;
    }

    public String getName(int position) {
        return this.names[position];
    }

    @Override
    public String getItem(int position) {
        return this.defs[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final String name = getName(position);
        final String def = getItem(position);


        if (convertView == null)
            convertView = LayoutInflater.from(context).inflate(R.layout.item_deflist, parent, false);

        ((TextView) convertView.findViewById(R.id.deflist_nametext)).setText(name);
        ((TextView) convertView.findViewById(R.id.deflist_deftext)).setText(def);

        return convertView;
    }
}