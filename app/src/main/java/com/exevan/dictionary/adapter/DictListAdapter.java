package com.exevan.dictionary.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.exevan.dictionary.R;
import com.exevan.dictionary.activities.DictionarySelectActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DictListAdapter extends BaseAdapter {

    Context context;

    Map<String, String> dicts;
    List<String> ids;
    boolean[] unselected;

    public DictListAdapter(Context context, Map<String, String> dicts) {
        this.context = context;
        this.dicts = dicts;
        this.ids = new ArrayList<>(dicts.keySet());
        this.unselected = new boolean[ids.size()];
    }

    @Override
    public int getCount() {
        return ids.size();
    }

    public String getKey(int position) {
        return ids.get(position);
    }

    @Override
    public String getItem(int position) {
        return dicts.get(getKey(position));
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final String id = getKey(position);
        final String name = getItem(position);


        if (convertView == null)
            convertView = LayoutInflater.from(context).inflate(R.layout.item_dictlist, parent, false);

        if (unselected[position])
            convertView.setBackground(null);
        else
            convertView.setBackground(context.getResources().getDrawable(R.drawable.item_dictlist_selectedborder));

        ((TextView) convertView.findViewById(R.id.dictlist_idtext)).setText(id);
        ((TextView) convertView.findViewById(R.id.dictlist_nametext)).setText(name);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                unselected[position] = !unselected[position];

                ((DictionarySelectActivity) context).unselectDict(id, v);
            }
        });

        return convertView;
    }
}