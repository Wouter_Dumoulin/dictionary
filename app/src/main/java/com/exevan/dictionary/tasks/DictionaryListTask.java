package com.exevan.dictionary.tasks;

import android.os.AsyncTask;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Exevan on 27/01/2017.
 */

public class DictionaryListTask extends AsyncTask<Void, Void, Map<String, String>> {

    @Override
    protected Map<String, String> doInBackground(Void... params) {
        try {
            SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

            //request.addProperty("prop1", "myprop");

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet=true;
            envelope.setOutputSoapObject(request);

            HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

            androidHttpTransport.call(SOAP_ACTION, envelope);

            SoapObject result = (SoapObject) envelope.getResponse();

            Map<String, String> dicts = new HashMap<>();

            for(int i = 0; i < result.getPropertyCount(); i++) {
                String id = ((SoapObject) result.getProperty(i)).getProperty("Id").toString();
                String name = ((SoapObject) result.getProperty(i)).getProperty("Name").toString();
                dicts.put(id, name);
            }

            return dicts;
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static final String SOAP_ACTION = "http://services.aonaware.com/webservices/DictionaryList";
    private static final String METHOD_NAME = "DictionaryList";
    private static final String NAMESPACE = "http://services.aonaware.com/webservices/";
    private static final String URL = "http://services.aonaware.com/DictService/DictService.asmx";
}
