package com.exevan.dictionary.tasks;

import android.os.AsyncTask;
import android.util.Log;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LookupTask extends AsyncTask<String, Void, Map<String, String>> {

    private Map<String, String> selectedDicts;

    public LookupTask(Map<String, String> selectedDicts) {
        this.selectedDicts = selectedDicts;
    }

    @Override
    protected Map<String, String> doInBackground(String... params) {
        Map<String, String> result = new HashMap<>();
        for (String id : selectedDicts.keySet()) {
            List<String> defs = getDefsFromDict(params[0], id);

            if(defs == null || defs.isEmpty())
                continue;

            for (String def : defs)
                result.put(selectedDicts.get(id), def);
        }
        return result;
    }

    private List<String> getDefsFromDict(String term, String id) {
        try {
            SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

            request.addProperty("dictId", id);
            request.addProperty("word", term);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

            androidHttpTransport.call(SOAP_ACTION, envelope);

            SoapObject result = (SoapObject) envelope.getResponse();
            SoapObject defResult = (SoapObject) result.getProperty("Definitions");

            if(defResult.getPropertyCount() == 0)
                return null;

            List<String> defs = new ArrayList<>();

            for (int i = 0; i < defResult.getPropertyCount(); i++) {
                String def = ((SoapObject) defResult.getProperty(i)).getProperty("WordDefinition").toString();
                Log.d("Dictionary", def);
                defs.add(def);
            }

            return defs;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static final String SOAP_ACTION = "http://services.aonaware.com/webservices/DefineInDict";
    private static final String METHOD_NAME = "DefineInDict";
    private static final String NAMESPACE = "http://services.aonaware.com/webservices/";
    private static final String URL = "http://services.aonaware.com/DictService/DictService.asmx";
}
